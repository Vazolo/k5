
import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
   }
   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
      Tnode t = Tnode.buildFromRPN ("2 -");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }

   @Test (timeout = 1000)
   public void testDup(){
      String rpn = "3 DUP *";
      Tnode result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "*(3,3)");
   }
   @Test (timeout = 1000)
   public void testSWAP(){
      String rpn = "2 5 SWAP -";
      Tnode result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "-(5,2)");
   }

   @Test (timeout = 1000)
   public void testROT(){
      String rpn = "2 5 9 ROT - +";
      Tnode result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "+(5,-(9,2))");
   }

   @Test (timeout = 1000)
   public void testComplexRPN(){
      String rpn = "2 5 9 ROT + SWAP -";
      Tnode result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "-(+(9,2),5)");

      rpn = "2 5 DUP ROT - + DUP *";
      result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "*(+(5,-(5,2)),+(5,-(5,2)))");

      rpn = "-3 -5 -7 ROT - SWAP DUP * +";
      result = Tnode.buildFromRPN(rpn);
      assertEquals(result.toString(), "+(-(-7,-3),*(-5,-5))");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbersDUP() {
      Tnode t = Tnode.buildFromRPN ("DUP *");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbersSWAP() {
      Tnode t = Tnode.buildFromRPN ("2 SWAP");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbersROT() {
      Tnode t = Tnode.buildFromRPN ("1 2 ROT");
   }

}
