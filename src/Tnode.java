import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public static void main (String[] param) {
      String rpn = "2 5 9 ROT - +";
      Tnode result = Tnode.buildFromRPN(rpn);
      System.out.println ("RPN: " + rpn);
      System.out.println(result);

//      Tnode res = buildFromRPN (rpn);
//      System.out.println ("Tree: " + res);
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      Tnode currentNode;
      Stack<Tnode> tnodeStack = new Stack<>();

      currentNode = this;
      boolean movingDown = true;
      boolean dontWrite = false;

      do {
         if(movingDown){

            if (currentNode.hasChild()){
               tnodeStack.push(currentNode);

               b.append(currentNode.name);
               b.append("(");
               currentNode = currentNode.getFirstChild();
            }
            else {
               movingDown = false;
            }

         }else {

            if (!dontWrite) {
               b.append(currentNode.name);
            }

            if (currentNode.hasSibling()){
               b.append(",");
               currentNode = currentNode.getSibling();
               dontWrite = false;
               movingDown = true;
            }

            else {
               b.append(")");
               currentNode = tnodeStack.pop();
               dontWrite = true;
            }
         }

      }while (tnodeStack.size() != 0);

      if (b.toString().equals("")){
         b.append(currentNode.name);
      }

      return b.toString();
   }

   private boolean hasChild() {
      return firstChild != null;
   }

   private Tnode shallowCopy(){
      Tnode copy = new Tnode();
      copy.setName(name);
      copy.setChild(getFirstChild());
      copy.setSibling(getSibling());
      return copy;
   }

   public static Tnode buildFromRPN (String pol) {
      Pattern pattern = Pattern.compile("\\S+");
      List<String> commands = new ArrayList<>();
      Matcher m = pattern.matcher(pol);
      while (m.find()) {
         commands.add(m.group());
      }

      Stack<Tnode> newStack = new Stack<>();
      for (String command : commands) {
         if (isNumeric(command)){
            Tnode newNode = new Tnode();
            newNode.setName(command);
            newStack.push(newNode);

         }

         else if (command.equals("DUP")){
            if (newStack.size() < 1){
               throw new RuntimeException("Wrong input: " + pol);
            }
            Tnode num = newStack.pop();
            Tnode copy = num.shallowCopy();
            newStack.push(num);
            newStack.push(copy);
         }

         else if (command.equals("SWAP")){
            if (newStack.size() < 2){
               throw new RuntimeException("Wrong input: " + pol);
            }
            Tnode num1 = newStack.pop();
            Tnode num2 = newStack.pop();

            newStack.push(num1);
            newStack.push(num2);
         }

         else if (command.equals("ROT")){
            if (newStack.size() < 3){
               throw new RuntimeException("Wrong input: " + pol);
            }
            Tnode num1 = newStack.pop();
            Tnode num2 = newStack.pop();
            Tnode num3 = newStack.pop();

            newStack.push(num2);
            newStack.push(num1);
            newStack.push(num3);

         }

         else if (legalOperations(command)){
            if (newStack.size() < 2) {
               throw new RuntimeException("Incorrect input: " + pol + ". It has to be two nums per operation.");
            }
               Tnode childSib = newStack.pop();
               Tnode child = newStack.pop();
               child.setSibling(childSib);

               Tnode newNode = new Tnode();
               newNode.setName(command);
               newNode.setChild(child);
               newStack.push(newNode);
         }
         else {
            throw new RuntimeException("Incorrect input: " + pol + ". Illegal operation: " + command);
         }

      }

      Tnode result = newStack.pop();

      if (newStack.size() != 0){
         throw new RuntimeException("Incorrect input: " + pol +". Too many nums");
      }
      return result;
   }

   private static boolean legalOperations(String command) {

      if ("+".equals(command) || "/".equals(command) || "*".equals(command) || "-".equals(command)) {
         return true;
      }
      return false;
   }

   private void setChild(Tnode child) {
      firstChild = child;
   }

   private void setSibling(Tnode sib) {
      nextSibling = sib;
   }

   private void setName(String command) {
      name = command;
   }

   private static boolean isNumeric(String str){
      try {
         Integer.parseInt(str);
      }
      catch (Exception e){
         return false;
      }
      return true;
   }

   private boolean hasSibling() {
      return getSibling() != null;
   }

   private Tnode getSibling() {
      return nextSibling;
   }

   private Tnode getFirstChild() {
      return firstChild;
   }
}